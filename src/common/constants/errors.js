export default {
    INVALID_TOKEN: 'invalid_token',
    INVALID_MOVE: 'invalid_move',
    INVALID_ACTION: 'invalid_action'
};
