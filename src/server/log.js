import bunyan from 'bunyan';

const log = bunyan.createLogger({
    name: 'jeu-de-carte'
});


export default log;
