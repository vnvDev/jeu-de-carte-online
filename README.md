# (Ce n'est pas) 6 Qui Prend !

## Description

- [Règles du jeu](http://www.gigamic.com/files/catalog/products/rules/rules-6quiprend-05-2012.pdf)
- [Modélisation](docs/data-modeling.md)
- [Vocabulaire anglais des jeux de carte](https://en.wikipedia.org/wiki/Glossary_of_card_game_terms)

## Pré-Requis
- [NodeJS](https://nodejs.org/)

## Technologies
### Client
- [Webpack](http://webpack.github.io/) + [Gulp](http://gulpjs.com/) pour builder la solution
- [React](https://facebook.github.io/react/) 
- [redux](http://rackt.github.io/redux/)

A l'avenir : 
- [socket.io](http://socket.io/) ?
(cf. http://teropa.info/blog/2015/09/10/full-stack-redux-tutorial.html)

### Server
- TODO

A l'avenir :
- [Express](http://expressjs.com/) + [socket.io](http://socket.io/) probablement

## Développement Front
```
$ npm i
$ npm run dev
```
